/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.dbdcoffeeproject.test_service;

import com.werapan.dbdcoffeeproject.model.Customer;
import com.werapan.dbdcoffeeproject.service.CustomerService;

/**
 *
 * @author Paweena Chinasri
 */
public class TestCustomerService {
    public static void main(String[] args) {
        CustomerService cs = new CustomerService();
        for(Customer customer :cs.getCustomers()){
            System.out.println(customer);
    }
        System.out.println(cs.getByTel("0926421645"));
        Customer cus1 = new Customer("Mod", "0756421789");
        cs.addNew(cus1);
        for(Customer customer :cs.getCustomers()){
            System.out.println(customer);
    }
        Customer delCus = cs.getByTel("0756421789");  
        delCus.setTel("0858368741");
        cs.update(delCus);
        System.out.println("After updated");
        for(Customer customer :cs.getCustomers()){
            System.out.println(customer);
    }
        cs.delete(delCus);
        for(Customer customer :cs.getCustomers()){
            System.out.println(customer);
}
}
}
