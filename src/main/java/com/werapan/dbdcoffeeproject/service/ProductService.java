/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.dbdcoffeeproject.service;

import com.werapan.dbdcoffeeproject.dao.ProductDao;
import com.werapan.dbdcoffeeproject.model.Product;
import java.util.ArrayList;

/**
 *
 * @author Paweena Chinasri
 */
public class ProductService {
    private ProductDao productDao = new ProductDao();
    public ArrayList<Product> getProductsOrderByName() {
        return (ArrayList<Product>) productDao.getAll(" product_name ASC");
    }
}
